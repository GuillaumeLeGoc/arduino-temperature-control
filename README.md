# Arduino Temperature Control
## Temperature regulation with an Arduino board.

Measures temperature with a thermocouple and, computes an error signal with respect to a target temperature and update the PWM frequency to mitigate power delivered to a Peltier module through a PID loop.

Along with the Arduino code, there is a simple GUI that interacts with the Arduino to read and plot temperature over time, set the target and PID coefficients. It can also log the temperature in a text file, and supports simple protocols to change target and coefficients dynamically. The protocol can be triggered through TCP/IP.

### Contents
- TemperatureController : Code to be flashed on the Arduino board.
- Python files (.py) : the user interface (gui.py) that relies on Arduino, Protocol and Server classes, defined in corresponding files.
- README.md : this file! Contains a quick setup guide for the electronics and GUI installation below.

### Side note
This method is used to read a temperature probe to stabilize temperature, but the error/PID/command pipeline should be general enough to handle any kind of in/out signals, with minimal change in the Arduino code and user interface.

### Hardcoded default values
1. Depending on the [thermocouple type](https://en.wikipedia.org/wiki/Thermocouple#Types), you need to manually change the .ino file (in `TemperatureController/TemperatureController.ino`), line 79 :
`TC.setThermocoupleType(MAX31856_TCTYPE_T);` (type T), `TC.setThermocoupleType(MAX31856_TCTYPE_K);` (type K), ...
2. Arduino pins for the thermocouple reading is hardcoded at line 22 of the .ino file. Arduino PWM output pins are hardcoded at lines 24-25. Change them acording to your wiring.
3. Arduino port, baudrate, timeout; temperature target, P, I, D coefficients and default paths are hardcoded within the `gui.py` line 25-36. You should change them to your need.

## Electronic setup
This project is meant to be used to drive a Peltier module through a H-Bridge whose inputs are two PWD-ready Arduino outputs. 
The error signal that will feed the PID loop is computed from a temperature sensor. Any should do, but we use a thermoucouple type K together with a thermocouple amplifier [Adafruit MAX31856](https://www.adafruit.com/product/3263).  
An H-Bridge is a standard electronic component used to drive the direction and speed of any kind of motors. In our case, the speed would be the heat power and the direction would be to heat or cool the top surface of the Peltier module. H-Bridges might be available for purchase, however, it is quite doable to do it yourself.  

### Temperature reading
Pinouts and assembly of the thermocouple amplifier are detailed in the [Adafruit documentation](https://learn.adafruit.com/adafruit-max31856-thermocouple-amplifier/pinouts), together with examples. We'll then feed the H-Bridge with the PWM pins of the Arduino board.

### H-Bridge
You'll need :
- A Peltier device
- A 12V/1-10A power supply (depending on the required power, a 12V/2A is enough to control temperature in an open 50mL water bath).
- 2 PNP Darlington Power Transistor (we use TIP147 PNP)
- 2 NPN Darlington Power Transistor (we use TIP142 NPN)
- 2 NPN Transistor (we use KSP2222A)
- 2 resistances 1kOhm
- 2 resistances 10kOhm

The following figure shows a schematic of the H-Bridge.

![Schematics of an H-Bridge](./Images/hbridge.svg "Schematic of the H-bridge")

Next figure is the wiring diagram to solder on a board (or whatever you prefer).

![Wiring diagram of the H-Bridge](./Images/hbridge_board.svg "Wiring diagram of the H-Bridge")

Keep in mind that the transistor might heat a lot, you should espace them from each other, and optionnaly add passive coolers to them.  
Then, the power supply should be plugged/soldered to the 12V/GND pins. HOT and COLD pins should be connected to two Arduino PWM (~) pins (according to the values set at lines 24-25 of the .ino file). The Peltier should be plugged/soldered to the Peltier pins.  
The next section describes how to get the Arduino code and GUI up and running.

## Installation

Download or clone this repo somewhere.

### Arduino setup
1. Install the Arduino IDE for your platform from the website : [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software).
2. Install required libraries from the Arduino Library Manager: [`Adafruit_MAX31856`](https://www.arduino.cc/reference/en/libraries/adafruit-max31856-library/), [`PID`](https://www.arduino.cc/reference/en/libraries/pid/), [`Chrono`](https://www.arduino.cc/reference/en/libraries/chrono/)
3. Plug-in your board (tested with a Leonardo one but any should do) through USB, set the baudrate to 115200 and timeout to 1.
4. Flash `TemperatureController.ino` to the board.
5. Check if it's working by monitoring serial output. It should display `Data A B C D`, with :
	- A : timestamp in milliseconds
	- B : Temperature in degrees Celsius (thermocouple reading)
	- C : Cold junction temperature in degrees Celcius (thermocouple amplifier thermistance reading)
	- D : PWM frequency (digital -255/255) sent to the Peltier.

### Graphical user interface

The GUI is written in python using the PyQt5 library. You need a working python installation with pip, which are installed in most of Linux distributions.

#### Linux

`pip install -r requirements.txt`  
`python gui.py`

#### Windows

1. Install [Chocolatey package manager](https://chocolatey.org/install#individual) :
	- Open PowerShell as admin. 
	- Run `Set-ExecutionPolicy AllSigned` to set the correct installation policy.
	- Run `Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))` to install chocolatey (answer yes to all).
2. Install python3
	- Run `choco install python`
	- Close & open PowerShell as admin.
	- Update pip : `python -m pip install --upgrade pip`
	- `cd` to this repo. 
	- Install required python libraries : `pip install -r requirements.txt`
	- It _should_ work. If a package causes problem, try to install it manually, _eg._ `pip install pyqtgraph`
3. `python gui.py` from regular PowerShell.
4. Optional : create a shortcut with .bat file (todo).

## (todo) Graphical user interface description

## Acknowledgements
- Raphaël Candelier for the initial development of the Arduino code (see [ThermoMaster](https://gitlab.com/GuillaumeLeGoc/thermomaster)).
- [Chris from pyroelectro.com for the electronic parts](http://www.pyroelectro.com/tutorials/h_bridge_4_transistor/index.html).
- [Zetcode tutorial to get started with PyQt5](https://zetcode.com/gui/pyqt5/).
- Original authors of the required Arduino and Python libraries.
